# yarn Auto Remediate examples.

This project contains several example projects, that show problems with auto-remidiating yarn.lock files.

## Easy remidiation within semver range

The [first use case](./00_easy) is the easy one. We have a direct dependency to a vulnerable dependency (`debug@2.6.8`):

-  `package.json`: 
    ```
    "debug": "^2.6.8"
    ```
-  `yarn.lock`: 
    ```
    debug@^2.6.8:
      version "2.6.8"
    ```

### Remidiation

Pretty straight forward, because we can just update `debug` to `2.6.9` and be done with it. We can either do it directly in the yarn.lock or in both `yarn.lock` and `package.json`

## Problematic major version

The [second use case](./01_major) is already harder. We have a direct dependency to a vulnerability (`jquery@1.9.1`). Only a major update (`>= 3.0.0`) can help

-  `package.json`: 
    ```
    "jquery": "1.9.1"
    ```
-  `yarn.lock`: 
    ```
    jquery@1.9.1:
      version "1.9.1"
    ```

### Remidiation

Simply upgrading to `jquery >= 3.0.0` is not possible, because it will probably break the application

## Deep fixed dependency

The [third use case](./02_deep_fixed) shows a bigger problem. One of our direct dependencies has a vulnerable dependency. The dependency does not specify a semver and uses a fixed one.

-  `package.json`: 
    ```
    "compression": "1.7.0"
    ```
-  `yarn.lock`: 
    ```
    debug@2.6.8:
      version "2.6.8"
    ```

### Remidiation

We cannot upgrade `debug` without upgrading `compression`.

## Interlocked dependencies

The [fourth use case](./03_interlocked) shows a bigger problem. This is a mixed case.

-  `package.json`: 
    ```
    "compression": "1.7.0",
    "undefsafe": "^2.0.2"
    ```
-  `yarn.lock`: 
    ```
    debug@2.6.8, debug@^2.2.0:
      version "2.6.8"
    ```

### Remidiation

We cannot upgrade `debug` without upgrading `compression` and `undefsafe`, same like (the third use case).